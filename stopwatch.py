#!/usr/bin/python3
"""Simple comand line stopwatch."""
import time
import sys

start_time = time.time()
script_start = start_time

print("Press [CTRL+C] to stop..")
print("Press [R] to reset the timer.")

def reset_timer():
    """Reset the start_time variable."""
    now = time.time()
    elapsed_time = now - start_time
    print(f"Interval time: {int(elapsed_time / 3600):02d}"
          f":{int(elapsed_time % 3600 / 60):02d}"
          f":{int(elapsed_time % 60):02d}")
    return now

try:
    while True:
        try:
            key = input().strip()
            if key.lower() == 'r':
                start_time = reset_timer()
            if key.lower() == 'q':
                raise EOFError
        except KeyboardInterrupt:
            break
        elapsed_time = time.time() - start_time
        print(f"\rElapsed time: {int(elapsed_time / 3600):02d}"
              f":{int(elapsed_time % 3600 / 60):02d}"
              f":{int(elapsed_time % 60):02d}\n", end="")
        time.sleep(1)

except EOFError:
    pass

elapsed_time = time.time() - script_start
print(f"\nTotal time: {int(elapsed_time / 3600):02d}"
      f":{int(elapsed_time % 3600 / 60):02d}"
      f":{int(elapsed_time % 60):02d}")
