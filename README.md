# Time utilities

This is a simple collection of bash and python utilities and helper functions.

## Timerzo

Takes a user input in minutes and count downs to 0 and then plays a bell sound

```sh
./timer.sh 5 
```

## Stopwatch

Starts a timer from the command line allows a user to record splits and prints
out the total time at the end.
