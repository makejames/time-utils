#!/bin/bash

move_to_path() {
    destination_dir="/usr/local/bin"
    # Check if the destination directory exists
    if [ ! -d "$destination_dir" ]; then
        echo "Error: Destination directory '$destination_dir' not found."
        exit 1
    fi

    # Move the script to the destination directory
    sudo cp "$1" "$destination_dir/$1"
    sudo mv "$destination_dir/$1" "$destination_dir/${1%.*}"

    echo "Script '$1' was successfully moved to '$destination_dir'."
}

move_to_path stopwatch.py
move_to_path timer.sh
