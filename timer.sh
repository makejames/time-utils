#!/bin/bash

duration=$1

for ((i=$duration*60; i>=0; i--)); do
  printf "\r%02d:%02d" $((i/60)) $((i%60))
  sleep 1
done

printf "\a\nTime's up! Take a break.\n"
